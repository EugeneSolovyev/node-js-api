const Sequelize = require('sequelize');
const DB_NAME = config.development.database;
const DB_USER = config.development.username;
const DB_PASS = config.development.password;
const DB_HOST = 'db';

global.DB = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
	host: DB_HOST,
	dialect: 'postgres',
	operatorsAliases: false,

	pool: {
		max: 5,
		min: 0,
		acquire: 30000,
		idle: 10000
	},
});

DB
.authenticate()
.then(() => {
	console.log('Connection has been established successfully.');
})
.catch(err => {
	console.error('Unable to connect to the database:', err);
});
