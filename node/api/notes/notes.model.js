const notesModel = require('./notes.schema.js').notes;

exports.create_note = (data, cb) => {
    new notesModel({
        title: data.title,
        text: data.body
    }).save((err, doc) => {
        cb(err, doc);
    })
}

exports.get_all = cb => {
    notesModel.find({}, (err, doc) => {
        cb(err, doc);
    })
}