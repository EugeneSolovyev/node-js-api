const NoteModel = require('./notes.model');

module.exports = {
    createNote,
    getAllNotes,
}

function createNote(req, res) {
    console.log('REQUEST', req);
    NoteModel.create_note(req.body, (err, response) => {
        if (err) {
            res.status(500).send({err});
            logger.log('error', err);
        }

        res.status(200).json(response);
    });
}

function getAllNotes(_, res) {
    NoteModel.get_all((err, response) => {
        if (err) return res.status(500).send({err});

        res.status(200).json(response);
    });
}