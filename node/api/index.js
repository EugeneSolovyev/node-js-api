const NotesController = require('./notes/notes.controller');

module.exports = (app, db) => {
    app.post('/create-note', NotesController.createNote);
    app.get('/get-all-notes', NotesController.getAllNotes);
}