FROM alpine:latest
MAINTAINER NGINX Docker For BostonFlatrate "develnk@gmail.com"
ENV NGINX_VERSION=1.13.3 \
 NGINX_ASC_URL="http://nginx.org/download/nginx-1.13.3.tar.gz.asc" \
 NGINX_GPG_KEYS="B0F4253373F8F6F510D42178520A9993A1C052F8" \
 OPEN_SSL_VERSION=1.0.2l \
 OPEN_SSL_ASC="https://www.openssl.org/source/openssl-1.0.2l.tar.gz.asc" \
 OPEN_SSL_SHA1="b58d5d0e9cea20e571d903aafa853e2ccd914138" \
 OPEN_SSL_SHA256="ce07195b659e75f4e1db43552860070061f156a98bb37b672b101ba6e3ddf30c" \
 ZLIB_VERSION=1.2.11 \
 PCRE_VERSION=8.41 \
 APP_DIR=/app

ENV CONFIG "\
    --prefix=/etc/nginx \
    --sbin-path=/usr/sbin/nginx \
    --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --pid-path=/var/run/nginx.pid \
    --lock-path=/var/run/nginx.lock \
    --http-client-body-temp-path=/var/cache/nginx/client_temp \
    --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
    --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
    --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
    --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
    --user=http \
    --group=http \
    --with-http_realip_module \
    --with-http_gunzip_module \
    --without-mail_pop3_module \
    --without-mail_imap_module \
    --without-mail_smtp_module \
    --without-stream_limit_conn_module \
    --without-stream_access_module \
    --without-stream_upstream_zone_module \
    --without-stream_upstream_zone_module \
    --without-stream_upstream_least_conn_module \
    --without-http_uwsgi_module \
    --without-http_memcached_module \
    --without-http_limit_conn_module \
    --without-http_limit_req_module \
    --without-http_empty_gif_module \
    --without-http_upstream_zone_module \
    --without-http_upstream_keepalive_module \
    --without-http_upstream_least_conn_module \
    --without-http_upstream_ip_hash_module \
    --without-http_upstream_hash_module \
    --without-http_ssi_module \
    --without-http_split_clients_module \
    --without-http_referer_module \
    --without-http_geo_module \
    --without-http_map_module \
    --without-http_memcached_module \
    --with-pcre=../pcre-$PCRE_VERSION \
    --with-http_gzip_static_module \
    --with-http_auth_request_module \
    --with-http_ssl_module \
    --with-openssl=../openssl-$OPEN_SSL_VERSION \
    --with-zlib=../zlib-$ZLIB_VERSION \
    --with-threads \
    --with-debug \
    "

ENV APK_PACKAGES "build-base perl linux-headers libxslt curl harfbuzz binutils fontconfig gc guile kbproto libatomic_ops libice libjpeg-turbo libpng libsm libtool libvpx libx11 libxau libxcb libxdmcp libxext libxpm libxt xcb-proto xextproto xproto gnupg"
ENV APK_ADD_PACKAGES "gd geoip libxml2"

RUN apk add --update $APK_PACKAGES $APK_ADD_PACKAGES

RUN addgroup -S http && adduser -S http -G http

RUN set -ex \
    && curl -fSL http://nginx.org/download/nginx-$NGINX_VERSION.tar.gz -o nginx.tar.gz \
	&& curl -fSL http://zlib.net/zlib-$ZLIB_VERSION.tar.gz -o zlib.tar.gz \
	&& curl -fSl https://www.openssl.org/source/openssl-$OPEN_SSL_VERSION.tar.gz -o openssl.tar.gz \
	&& curl ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-$PCRE_VERSION.tar.gz -o pcre.tar.gz \
    && curl -fSL $NGINX_ASC_URL -o nginx.tar.gz.asc \
    && export GNUPGHOME="$(mktemp -d)" \
    && found=''; \
        for server in \
            ha.pool.sks-keyservers.net \
            hkp://keyserver.ubuntu.com:80 \
            hkp://p80.pool.sks-keyservers.net:80 \
            pgp.mit.edu \
        ; do \
            echo "Fetching GPG key $NGINX_GPG_KEYS from $server"; \
            gpg --keyserver "$server" --keyserver-options timeout=10 --recv-keys "$NGINX_GPG_KEYS" && found=yes && break; \
        done; \
    test -z "$found" && echo >&2 "error: failed to fetch GPG key $NGINX_GPG_KEYS" && exit 1; \
    gpg --batch --verify nginx.tar.gz.asc nginx.tar.gz \
    && rm -r "$GNUPGHOME" nginx.tar.gz.asc \
    && SHA256=$(sha256sum -b openssl.tar.gz|awk '{print$1}') \
    && if [ $SHA256 == $OPEN_SSL_SHA256 ]; then \
         echo "OPENSSL SHA256 KEY is verify "; else echo "Wrong verify SHA256 KEY!!!"|exit 25; \
    fi \
    && SHA1=$(sha1sum -b openssl.tar.gz|awk '{print$1}') \
    && if [ $SHA1 == $OPEN_SSL_SHA1 ]; then \
         echo "OPENSSL SHA1 KEY is verify "; else echo "Wrong verify SHA1 KEY!!!"|exit 25; \
    fi \
	&& mkdir -p /usr/src \
	&& mkdir -p /var/cache/nginx/client_temp \
	&& mkdir /app \
	&& chown -R http:http /app \
	&& chmod -R 765 /app \
	&& tar -zxC /usr/src -f nginx.tar.gz \
	&& tar -zxC /usr/src -f zlib.tar.gz \
	&& tar -zxC /usr/src -f openssl.tar.gz \
	&& tar -zxC /usr/src -f pcre.tar.gz \
	&& rm *.tar.gz \
	&& cd /usr/src/nginx-$NGINX_VERSION \
	&& ./configure $CONFIG  \
	&& make -j$(nproc) \
	&& make install

COPY nginx.conf /etc/nginx/nginx.conf
COPY htpasswd /etc/nginx/conf/htpasswd

RUN rm -rf /usr/src/* \
    && apk del $APK_PACKAGES \
    && rm -rf /var/cache/apk/* /usr/share/doc/ /usr/share/man

WORKDIR $APP_DIR

EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]
